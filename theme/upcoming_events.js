
xmlhttp_init();

var upcoming_events = new Array();
var upcoming_events_order = new Array();  // because javascript can't sort a hash.  Stupid javascript.
var all_calendars = new Array();
var local_timezone_offset = new Date().getTimezoneOffset()*60000;

function plans_upcoming_events(properties) {
	this.id = get_property(properties, 'id', 'ue0');
  this.background_calendars_mode = get_property(properties, 'background_calendars_mode', 'menu');
	this.plans_url = get_property(properties, 'plans_url', '');
	this.days_before = get_property(properties, 'days_before', '30');
	this.days_after = get_property(properties, 'days_after', '0');
	this.cal_ids_string = get_property(properties, 'cal_ids', '0');
	this.merge_days = get_property(properties, 'merge_days', false);
	this.include_details = get_property(properties, 'include_details', false);
	this.debug = get_property(properties, 'debug', false);
  this.cal_ids = this.cal_ids_string.split(',');
  this.plans_theme_url = '';  // supplied along with events

	this.anchor = get_property(properties, 'list_element', null);
	this.menu_anchor = get_property(properties, 'menu_element', null);
  this.inited = false;
  this.current_calendar = 0;
   
  this.init = function() {
  
    if (document.getElementById(this.anchor)) {
      this.anchor_element = document.getElementById(this.anchor)
    } else {
      alert('unable to attach upcoming_events to anchor element with id: '+this.anchor);
      return;
    }
    
    if (document.getElementById(this.menu_anchor))
      this.menu_anchor_element = document.getElementById(this.menu_anchor)
      
    this.inited = true;
    
    //alert(this.anchor_element.tagName);
      
  }
  
  this.load = function() {
  
    if (!page_loaded) {
      this.load_later();
      return false;
    }
    
    var url = this.plans_url+'?get_upcoming_events=1';
    url+='&cal_ids='+this.cal_ids_string;
    url+='&days_before='+this.days_before;
    url+='&days_after='+this.days_after;
    url+='&background_calendars_mode='+this.background_calendars_mode;
    url+='&upcoming_events_id='+this.id;;
    
    
    if (this.debug) debug('upcoming events url:\n'+url);
    this.new_script = document.createElement('script');
    this.new_script.type = 'text/javascript';
    this.new_script.id = 'upcoming_events_script';
    this.new_script.defer = true;
    this.new_script.src = url;
    
    document.getElementsByTagName('head').item(0).appendChild(this.new_script);
    
    return;
  }
  
  
  this.load_later = function() {
    setTimeout(this.id+'.load()',200);
  }
  
  
  this.menusort = function(a, b) {
    
    if (!temp_cal_ids) return 0;
    //alert(temp_cal_ids);
    
	  if (array_indexof(temp_cal_ids,a) > array_indexof(temp_cal_ids,b)) return 1;
	  if (array_indexof(temp_cal_ids,a) < array_indexof(temp_cal_ids,b)) return -1;
	  return 0;
  }

  this.show = function() {
    if (!this.inited) this.init();
    var menu_results = this.generate_menu();
    //alert(menu_results);
    this.menu_anchor_element.innerHTML = menu_results;
    this.draw();
  }
  
  
  this.generate_menu = function() {
    
    // sort calendars in the order they were supplied.
    var temp_this = this;  // javascript is weird - the inline function isn't part of the class, so it doesn't know 'this'.
    
    all_calendars.sort(function(a, b) {
	    if (array_indexof(temp_this.cal_ids,a+'') > array_indexof(temp_this.cal_ids,b+'')) return 1;
	    if (array_indexof(temp_this.cal_ids,a+'') < array_indexof(temp_this.cal_ids,b+'')) return -1;
	    return 0;
    });
  
    if (this.background_calendars_mode == 'one_big_list') return ''; // one big list (no menu)
    
    var results = '';
    //results += '<select id="upcoming_events_calendar_select" onChange="setTimeout(\''+this.id+'.draw()\',2000)">';
    results += '<select id="upcoming_events_calendar_select" onChange="'+this.id+'.draw()">';
    
    for (var i=0;i<all_calendars.length;i++) {
      var calendar_id = all_calendars[i];
      if (this.background_calendars_mode == 'none' || this.background_calendars_mode == 'list')  // no background calendars or background calendars in list
        if (!array_contains(this.cal_ids,calendar_id))
          continue;
      else if (this.background_calendars_mode == 'menu') // background calendars in menu
        if (!array_contains(all_calendars,calendar_id))
          continue;
    

      var calendar = calendars[calendar_id];
      results += '<option value="'+calendar.id+'">'+calendar.title+'</option>';
    }
    results += '<select>';
    return results;
  }
  
  
  
  this.draw = function() {
    //alert(document.getElementById('upcoming_events_calendar_select'));
    //alert(document.getElementById('upcoming_events_calendar_select').selectedIndex);
    //alert(document.getElementById('upcoming_events_calendar_select')[document.getElementById('upcoming_events_calendar_select').selectedIndex].value);
    
    var select_element = document.getElementById('upcoming_events_calendar_select');
    
    if (select_element && select_element.selectedIndex != -1)
      this.current_calendar = select_element[select_element.selectedIndex].value;
    else
      this.current_calendar = this.cal_ids[0];
    
    var results = this.generate_upcoming_events();
    if (!this.anchor_element)  {
      alert('(draw) anchor_element not defined.');
      return;
    }
    
    
	  if (this.anchor_element.tagName.toLowerCase() == 'table') {
		  if (this.anchor_element.outerHTML)  {  // IE doesn't support innerHTML for tables.  stupid, stupid IE.
				results = '<table id="upcoming_events_list" class="upcoming_events">'+results+'</table>';
			  this.anchor_element.outerHTML = results;
        this.anchor_element = document.getElementById(this.anchor)
      }
      else {
			  this.anchor_element.innerHTML = results;
      }
		}
		else
			this.anchor_element.innerHTML = results;
  }
  
  
  
  this.generate_upcoming_events = function() {
    var results = '';
     
    upcoming_events_order.sort(upcoming_eventssort);
    
    var day_merge_events = new Array();
    var day_merge_events_order = new Array();
    var current_day = -1;
    
    for (var upcoming_events_id in upcoming_events_order) {
      var event = upcoming_events[upcoming_events_order[upcoming_events_id]];
      if (!event || !event.cal_ids) continue;
      
      if (this.background_calendars_mode == 'one_big_list') {} //do nothing
      
      else if (this.background_calendars_mode == 'none' || this.background_calendars_mode == 'menu') {  // no background calendars
          if (!array_contains(event.cal_ids,this.current_calendar))
            continue;
      } else if (this.background_calendars_mode == 'list') {
        var found = false;
        if (array_contains(event.cal_ids,this.current_calendar))
          found = true;
        else if (this.current_calendar.local_background_calendars)
          for (var i=0;i<this.current_calendar.local_background_calendars.length;i++) {
            if (array_contains(event.cal_ids,this.current_calendar.local_background_calendars[i]))
            found = true;
            break;
          }
        if (!found) continue;
      } // background calendar events in list
        
      // display the event
      
      var date_string;
      var weekday_string;
      var nice_event_date = '';
      var nice_event_time = ''
      var link_text = (event.details_url) ? event.details : 'javascript:'+this.id+'.display_event(\''+event.id+'\')';
      
      
      if (event.all_day_event) {
        nice_event_date = nice_date(event.start, event.start*1+86400*(event.days-1), true, false);
        nice_event_time = '';
      } else {
        nice_event_date = nice_date(event.start, event.end, true, false);
        nice_event_time = nice_time(event.start, event.end);
      }
      
      if (this.anchor_element.tagName.toLowerCase() == 'ul') {
        results += '<li>';
        results += '<a href="'+link_text+'">';
        results += '<span class="date">'+nice_event_date+'</span>';

        results += '<span class="title" style="background-color:'+event.bgcolor+';">';
 
        if (event.icon && event.icon != "blank")
          results += '<img class="icon" src="'+this.plans_theme_url+'/icons/'+event.icon+'_16x16.gif"/>';
 
        results += event.title+'</span></a>';
        if (this.include_details) results += upcoming_events_format_details(event.details);

        results += '</li>'
      } else if (this.anchor_element.tagName.toLowerCase() == 'table') {
      
        if (this.merge_days) {
          var event_day = new Date(event.start*1000).getUTCDate();
          if (!day_merge_events[event_day]) {
            day_merge_events[event_day] = new Array();
            day_merge_events_order.push(event_day);
          }
          day_merge_events[event_day].push(event.id);
        
        } else {
          results += '<tr><td class="date">';
 
          results += '<a href="'+link_text+'">';
          results += '<span class="date">'+nice_event_date+'</span>';
 
          results += '</td><td class="title">';
 
          results += '<a href="'+link_text+'">';
          results += '<span class="title" style="background-color:'+event.bgcolor+';">';
 
          if (event.icon && event.icon != "blank")
            results += '<img class="icon" src="'+this.plans_theme_url+'/icons/'+event.icon+'_16x16.gif"/>';
 
          results += nice_event_time+' '+event.title+'</span></a>';
          if (this.include_details) results += upcoming_events_format_details(event.details);

          results += '</td></tr>'
        }
      }
    }
    
    if (this.merge_days) {
      
      if (this.anchor_element.tagName.toLowerCase() == 'table') {
        for (var day_index in day_merge_events_order) {
          //alert(day_merge_events_order[day_index]);
          //alert(day_merge_events[day_merge_events_order[day_index]][0]);
 
          var temp_event = upcoming_events[day_merge_events[day_merge_events_order[day_index]][0]];
          var nice_event_date = formatDate(new Date(temp_event.start*1000), "dna MNA dd");
          
          results += '<tr><td class="merged_events date">';
          results += '<span class="date">'+nice_event_date+'</span>';
 
          results += '</td><td class="merged_events">';
          var merged_events = day_merge_events[day_merge_events_order[day_index]];
            for (var merged_event_id in merged_events) {
              var merged_event = upcoming_events[day_merge_events[day_merge_events_order[day_index]][merged_event_id]];
 
              var nice_event_time = (merged_event.all_day_event) ? '': nice_time(merged_event.start, merged_event.end);
              
              var link_text = (merged_event.details_url) ? merged_event.details : 'javascript:'+this.id+'.display_event(\''+merged_event.id+'\')';
              
              results += '<div class="merged_event"><a href="'+link_text+'">';
              results += '<span class="title" style="background-color:'+merged_event.bgcolor+';">';
 
              if (event.icon && event.icon != "blank")
                results += '<img class="icon" src="'+this.plans_theme_url+'/icons/'+merged_event.icon+'_16x16.gif"/>';
                results += nice_event_time+' '+merged_event.title+'</span></a>';
                if (this.include_details) results += upcoming_events_format_details(merged_event.details);
                results += '</div>';
            }
        }
        results += '</td></tr>'
      } else{
          // merged days isn't supported for list mode yet.
      }
    }
    return results;
  }
  
  this.display_event = function(evt_id) {
    var info_window_x = window_x()-info_window_width;
    var info_window_y = window_y();
 
    if (evt_id.match(/^r/)) {// remote event
      // do nothing.  Haven't figured this out yet.
    }else {  // local event
      var URL_string = this.plans_url+"?view_event=1&evt_id="+evt_id;
      info_window = window.open(URL_string, "info_window", "resizable=yes,status=yes,scrollbars=yes,top="+info_window_y+",left="+info_window_x+",width="+info_window_width+",height="+info_window_height);
    }
    info_window.focus();
  }
  
}

function upcoming_events_format_details(text) {
  var results = '<br/>'+text;
  return results;
}



function upcoming_eventssort(a, b) {
	//if (!a || !a.start) return -1;
	//if (!b || !b.start) return 1;

	if (upcoming_events[a].start < upcoming_events[b].start) return -1;
	if (upcoming_events[a].start > upcoming_events[b].start) return 1;	
	
	return 0;
}


